import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appchat/const.dart';
import 'package:flutter_appchat/pages/messages_page.dart';
import 'package:flutter_appchat/widgets/app_filled_outline_button.dart';
import 'package:flutter_appchat/widgets/app_text_bold.dart';
import 'package:flutter_appchat/widgets/app_text_normal.dart';
import 'package:flutter_appchat/widgets/app_text_title.dart';

import '../../model/Chat.dart';

class ChatsPage extends StatefulWidget {
  const ChatsPage({Key? key}) : super(key: key);

  @override
  State<ChatsPage> createState() => _ChatsPageState();
}

class _ChatsPageState extends State<ChatsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: TextTitle(
          text: "Chats",
          color: kContentColorDarkTheme,
          size: 24,
        ),
        backgroundColor: kPrimaryColor,
        actions: [
          Container(
              padding: const EdgeInsets.only(right: kDefaultPadding),
              child: const Icon(Icons.search))
        ],
      ),
      body: Container(
          child: Column(
        children: [
          _myMenuChat(),
          _myListChat(),
        ],
      )),

      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: kPrimaryColor,
        child: Icon(
          Icons.person_add_alt_1,
          color: Colors.white,
        ),
      ),
    );
  }
}

class _myMenuChat extends StatelessWidget {
  const _myMenuChat({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: kDefaultPadding),
      height: kDefaultPadding * 3,
      width: double.maxFinite,
      color: kPrimaryColor,
      child: Row(
        children: [
          FillOutlineButton(press: () {}, text: "Recent Message"),
          const SizedBox(
            width: kDefaultPadding,
          ),
          FillOutlineButton(
            press: () {},
            text: "Active",
            isFilled: false,
          )
        ],
      ),
    );
  }
}

class _myListChat extends StatelessWidget {
  const _myListChat({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          itemCount: chatsData.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: (){
                Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  MessagesPage(chat: chatsData[index],),
                            ),
                          );
                
              },
              child: Container(
                padding: const EdgeInsets.only(
                    left: kDefaultPadding,
                    top: kDefaultPadding / 2,
                    right: kDefaultPadding / 2),
                child: Row(
                  children: [
                    Stack(
                      children: [
                        Positioned(
                          child: CircleAvatar(
                            radius: kDefaultPadding * 3 / 2,
                            backgroundImage:
                                AssetImage(chatsData[index].image),
                          ),
                        ),
                        if (chatsData[index].isActive)
                          Positioned(
                            right: 0,
                            bottom: 0,
                            child: Container(
                              height: 20,
                              width: 20,
                              decoration: BoxDecoration(
                                color: kPrimaryColor,
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: Theme.of(context)
                                        .scaffoldBackgroundColor,
                                    width: 3),
                              ),
                            ),
                          )
                      ],
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: kDefaultPadding / 2),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextBold(
                              text: chatsData[index].name,
                              size: 16,
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .color!
                                  .withOpacity(0.64),
                            ),
                            const SizedBox(height: 5),
                            Text(
                              chatsData[index].lastMessage,
                              style: TextStyle(color: kGrey),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Text(
                      chatsData[index].time,
                      style: TextStyle(color: kGrey),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
