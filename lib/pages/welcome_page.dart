import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appchat/const.dart';
import 'package:flutter_appchat/pages/sign_in_page.dart';
import 'package:flutter_appchat/widgets/app_text_bold.dart';
import 'package:flutter_appchat/widgets/app_text_title.dart';
import 'package:flutter_appchat/widgets/app_text_normal.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: kDefaultPadding * 2),
        child: Column(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Image(image: AssetImage("assets/image/welcome_image.png")),
            const SizedBox(
              height: kDefaultPadding * 3 / 2,
            ),
            TextBold(
              text: "Welcome to our freedom \nmessaging app",
              size: 24,
              color: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .color!
                  .withOpacity(0.64),
            ),
            const SizedBox(
              height: kDefaultPadding * 5 / 2,
            ),
            TextNormal(
              text: "Freedom talk any person of your \nmother language.",
              size: 16,
              color: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .color!
                  .withOpacity(0.64),
            ),
            Expanded(
              child: Container(),
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const SignInPage(),
                  ),
                );
              },
              child: Container(
                  width: 100,
                  padding: const EdgeInsets.only(bottom: kDefaultPadding),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextNormal(
                        text: "Skip",
                        color: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .color!
                            .withOpacity(0.64),
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        color: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .color!
                            .withOpacity(0.64),
                      )
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
