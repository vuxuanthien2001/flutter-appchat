import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appchat/const.dart';
import 'package:flutter_appchat/pages/home_page.dart';
import 'package:flutter_appchat/widgets/app_text_normal.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: kDefaultPadding * 5,
                  width: kDefaultPadding * 5,
                  child: Image.asset(
                    MediaQuery.of(context).platformBrightness ==
                            Brightness.light
                        ? "assets/image/Logo_light.png"
                        : "assets/image/Logo_dark.png",
                  ),
                ),
                const SizedBox(
                  height: kDefaultPadding * 3,
                ),
                InkWell(
                  onTap: (() {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const HomePage(),
                      ),
                    );
                  }),
                  child: Container(
                    height: kDefaultPadding * 5 / 2,
                    width: kDefaultPadding * 25 / 2,
                    decoration: BoxDecoration(
                        color: kPrimaryColor,
                        borderRadius: BorderRadius.circular(kDefaultPadding)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextNormal(
                          text: "Sign in",
                          size: kDefaultPadding,
                          color: kContentColorDarkTheme,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: kDefaultPadding,
                ),
                Container(
                  height: kDefaultPadding * 5 / 2,
                  width: kDefaultPadding * 25 / 2,
                  decoration: BoxDecoration(
                      color: kSecondaryColor,
                      borderRadius: BorderRadius.circular(kDefaultPadding)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextNormal(
                        text: "Sign up",
                        size: kDefaultPadding,
                        color: kContentColorDarkTheme,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
