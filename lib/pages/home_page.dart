import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appchat/pages/chats_page/calls_page.dart';
import 'package:flutter_appchat/pages/chats_page/chats_page.dart';
import 'package:flutter_appchat/pages/chats_page/people_page.dart';
import 'package:flutter_appchat/pages/chats_page/profile_page.dart';

import '../const.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedIndex = 0;

  final Widget _myChats = ChatsPage();
  final Widget _myPeople = PeoplePage();
  final Widget _myCalls = CallsPage();
  final Widget _myProfile = ProfilePage();

  Widget getBody() {
    if (selectedIndex == 0) {
      return _myChats;
    } else if (selectedIndex == 1) {
      return _myPeople;
    } else if (selectedIndex == 2) {
      return _myCalls;
    } else {
      return _myProfile;
    }
  }

  void onTap(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getBody(),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: kPrimaryColor,
        type: BottomNavigationBarType.fixed,
        currentIndex: selectedIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.message),
            label: 'Chats',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            label: 'People',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.call),
            label: 'Calls',
          ),
          BottomNavigationBarItem(
            icon: CircleAvatar(
              radius: 14,
              backgroundImage: AssetImage("assets/image/user.png"),
            ),
            label: 'Profile',
          ),
        ],
        onTap: (int index) {
          onTap(index);
        },
      ),
    );
  }
}
